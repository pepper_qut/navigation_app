<?xml version="1.0" encoding="UTF-8" ?>
<Package name="Navigation" format_version="4">
    <Manifest src="manifest.xml" />
    <BehaviorDescriptions>
        <BehaviorDescription name="behavior" src="behavior_1" xar="behavior.xar" />
    </BehaviorDescriptions>
    <Dialogs />
    <Resources>
        <File name="styles" src="html/css/styles.css" />
        <File name="index" src="html/index.html" />
        <File name="jquery-3.2.1.min" src="html/js/jquery-3.2.1.min.js" />
        <File name="navigation" src="html/js/navigation.js" />
        <File name="StopSign" src="html/images/StopSign.png" />
        <File name="modal" src="html/css/modal.css" />
        <File name="icon" src="icon.png" />
    </Resources>
    <Topics />
    <IgnoredPaths />
    <Translations auto-fill="en_US">
        <Translation name="translation_en_US" src="translations/translation_en_US.ts" language="en_US" />
    </Translations>
</Package>
