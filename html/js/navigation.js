/* globals $, QiSession */

var behaviourManager = false
var navigationManager = false

var STATUS = {
  STARTED: 1,
  SUCCEEDED: 2,
  FAILED: 3,
  ABORTED: 4,
  UNKNOWN: 5
}

var clickEventType = ((document.ontouchstart !== null) ? 'click' : 'touchstart')

$(document).ready(function () {
  new QiSession(function (session) {
    connected(session)
  }, disconnected)

  $('#exit').on(clickEventType, function () {
    if (!behaviourManager) return
    behaviourManager.stopBehavior('User/navigation/behavior_1')
  })
  $('#stop').on(clickEventType, function () {
    if (!navigationManager) return
    navigationManager.stop()
  })
  $('#modal_close').on(clickEventType, function () {
    $('#localisation_modal').hide()
  })
  $(window).on(clickEventType, function (evt) {
    if (evt.target == $('#localisation_modal').get(0)) {
      $('#localisation_modal').hide()
    }
  })

  $('#localise').on(clickEventType, function () {
    var modal_contents = $('#localisation.modal-body')
    modal_contents.html('')

    $('#locations > .button').each(function (idx, to_copy) {
      var button = $(to_copy).clone(false)
      button.on(clickEventType, function () {
        navigationManager.setLocation(button.attr('data-location-id'))
      })
      modal_contents.append($('<p>'), button, $('</p>'))
    })
    $('#localisation_modal').show()
  })
})

function connected (session) {
  console.log('Connected')

  session.service('ALBehaviorManager').then(function (service) {
    behaviourManager = service
  })
  session.service('Navigation').then(function (service) {
    navigationManager = service

    navigationManager.status.connect(function (status) {
      if (status == STATUS.STARTED) {
        $('#stop').show()
      } else {
        $('#stop').hide()
      }
    });

    navigationManager.getLocations().then(function (locations) {
      $('#locations').html('')

      var keys = Object.keys(locations)
      keys.sort()

      keys.forEach(function (key) {
        var button = $('<div data-location-id="' + key + '" class="button">' + locations[key] + '</div>')

        button.on(clickEventType, function () {
          if (!navigationManager) return
          navigationManager.goto(key)
        })

        $('#locations').append($('<p>'), button, $('</p>'))
      })

      $('#loading').hide()
      $('#content').show()
    })
  })
}

function disconnected () {
  console.log('Disconnected...')
}
